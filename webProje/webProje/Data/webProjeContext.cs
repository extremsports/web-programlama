﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using webProje.Models;

namespace webProje.Data
{
    public class webProjeContext:DbContext//DbContext sinifinin ozellliklerini kullanmis oluyoruz
    {
        //veri tabanimizda tablo olarak temsil edilecek tum siniflarimizi cagirdik.
        public DbSet<Uye> Uyeler{ get; set; }
        public DbSet<Yorum> Yorumlar { get; set; }
        //hangi veri tabanina nasil yazilacagini web configle ekliyoruz
    }
}