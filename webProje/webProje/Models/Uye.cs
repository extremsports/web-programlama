﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using webProje.Data;

namespace webProje.Models
{
    public class Uye
    {
        public int uyeID { get; set; }//Tablo adinin sonuna ID eklendiginde framework otomatik olarak primary  key yapar
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public string EPosta { get; set; }
        [Required(ErrorMessage = "Lütfen adınızı giriniz.")]
        public string TelNO { get; set; }
        public string KullaniciAdi { get; set; }
        public string sifre { get; set; }
        //bir uyenin birden cok yorumu olabilecegi icin list icine aldik
        public virtual ICollection<Yorum> Yorumlar { get; set; }
    }
}